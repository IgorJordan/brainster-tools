<?php

use App\Http\Controllers\AdminController;
use Facade\Ignition\Middleware\AddQueries;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TechController@index')->name('main');
Route::get('/dynamical', 'TechController@dynamical')->name('dynamical');
Route::post('/live-search', 'TechController@action')->name('live_search');
Route::get('/courses/{id}', 'CourseController@index')->name('courses');
Route::post('/courses-save', 'CourseController@store')->name('courseStore');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['auth', 'role:admin'])->group(function() {
    Route::get('brainster/admin/panel', 'AdminController@show')->name('adminPanel');
    Route::get('brainster/admin/panel/accept/{id}', 'AdminController@accept')->name('accept');
    Route::get('brainster/admin/panel/reject/{id}', 'AdminController@reject')->name('reject');
    Route::get('brainster/admin/panel/delete/{id}', 'AdminController@delete')->name('delete');
});