@extends('layouts.project')
@section('content')
<div class="container">
    <div class="row justify-content-center" style="margin-top:50px">
        <div class="col-md-12 ">
            <table class="table table-bordered">
                <thead>
                <tr class="table-header">
                    <th>Course</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($courses as $course )
                    <tr class="{{ $course->status }}">
                        <td>
                            <div class="course-card course-side">
                                <div class="col-md-1">
                
                                    <div class="btn-group square" data-toggle="buttons">
                                        <label class="btn btn-primary active">
                                            <input type="checkbox" name="" id="" autocomplete="off">
                                            <i class="fas fa-caret-up"></i>
                                            <h4>{{ $course->like }}</h4>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-11 course-pad">
                                    <a class="course-title" href="{{ $course->url }}" target="_blank">
                                        <h4>{{ $course->title }} 
                                            <span class="grey">({{ $course->platform }})</span>
                                        </h4>
                                    </a>
                                    <div class="info-part">
                                        <span><a href=""><i class="fas fa-bookmark"></i><b> Bookmark</b></a></span>
                                        <span>Submited by <a href=""><b>{{ $course->user->name }}</b></a></span>
                                        <span><i class="fas fa-genderless"></i></span>
                                        <span>Views</span>
                                        <span><i class="fas fa-genderless"></i></span>
                                        <span>Discuss</span>
                                    </div>
                                    <div class="tags">
                                        <a href=""><b>{{ $course->type->name }}</b></a>
                                        <a href=""><b>{{ $course->level->name ?? ''}}</b></a>
                                        <a href=""><b>{{ $course->medium->name ?? '' }}</b></a>
                                        <a href=""><b>{{ $course->version->name ?? ''}}</b></a>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td><b>{{ $course->status }}</b></td>
                        <td class="buttons">
                            @if($course->status == 'unresponded')
                                <a href="{{ route('reject', ['id' => $course->id]) }}" class="btn btn-md btn-danger btn-block">Reject</a>
                                <a href="{{ route('accept', ['id' => $course->id]) }}" class="btn btn-md btn-success btn-block">Accept</a>
                            @elseif($course->status == 'accepted')
                                <a href="{{ route('reject', ['id' => $course->id]) }}" class="btn btn-md btn-danger btn-block">Reject</a>
                                <a href="{{ route('delete', ['id' => $course->id]) }}" class="btn btn-md btn-danger btn-block">Delete</a>
                            @elseif($course->status == 'rejected')
                                <a href="{{ route('accept', ['id' => $course->id]) }}" class="btn btn-md btn-success btn-block">Accept</a>
                                <a href="{{ route('delete', ['id' => $course->id]) }}" class="btn btn-md btn-danger btn-block">Delete</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{$courses->links()}}
    </div>
</div>
@endsection