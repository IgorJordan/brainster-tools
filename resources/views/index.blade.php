@extends('layouts.project')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h1>Find the Best Programming Courses & Tutorials</h1>
        </div>
    </div>
    <div class="row search-section">
        <div class="col-md-12 form-group">
            <i class="fas fa-search icon"></i>
            <input type="text" class="form-control search-input" name="search" id="search" placeholder="Search for the language you want to learn: Python, JavaScript...">
        </div>
    </div>
    <div class="row card-section" id="searchList">
        @foreach ($teches as $tech)
        <a href="{{ route('courses', ['id' => $tech->id]) }}">
        <div class="col-md-4">
            <div class="card">
                <img class="card-logo" src="{{ asset("images/logos/$tech->logo") }}" alt="program_image">
                <span> {{ $tech->name }}</span>
            </div>
        </div>
        </a>
        @endforeach
    </div>
</div>
@endsection
        
        