<div id="submitModal" class="modal fade new-modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
  
      <!-- Modal content-->

        <div class="modal-content">

            <div class="modal-body text-center">
                <button type="button" class="close modal-close m-submit" data-dismiss="modal"><i class="fas fa-times"></i></button>
                <h3 class="modal-title">Submit new tutorial</h3>
                <div class="s-line"></div>
                <form action="{{ route('courseStore') }}" method="POST" class="sub">
                @csrf
                
                <div class="form-group">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="platform" name="platform" placeholder="Platform" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="url" name="url" placeholder="URL" required>
                </div>
                <div class="form-group">
                    <select class="form-control select-color" id="tech" name="tech" required>
                        <option value="" class="grey">Select technology</option>
                        @foreach ($teches as $tech)
                        <option value="{{ $tech->id }}">{{ $tech->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control select-color" id="type" name="type" required>
                        <option value="" class="grey">Select type of course</option>
                        @foreach ($types as $type)
                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control select-color" id="medium" name="medium">
                        <option value="" class="grey">Select course medium</option>
                        @foreach ($media as $medium)
                        <option value="{{ $medium->id }}">{{ $medium->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control select-color" id="level" name="level">
                        <option value="" class="grey">Select course level</option>
                        @foreach ($levels as $level)
                        <option value="{{ $level->id }}">{{ $level->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control select-color" id="version" name="version">
                        <option value="">Select technology version</option>
                        {{-- @foreach ($versions as $version)
                        <option value="{{ $version->id }}">{{ $version->name }}</option>
                        @endforeach --}}
                    </select>
                </div>
                
                    <button onClick="myFunction()" type="submit" class="btn btn-submit form-control sub" id="save"><b>Save</b></button>
                </form>

            </div>
        </div>
  
    </div>
</div>