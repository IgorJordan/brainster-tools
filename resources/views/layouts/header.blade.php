@include('layouts.head')
    <body @if(Request::is("/")) class="" @elseif(Request::is("courses")) class="bkg-color" @endif>
        <nav class="navbar navbar-default nav-custom" role="navigation">
             <!-- Brand and toggle get grouped for better mobile display -->

            {{-- Admin-panel navbar --}}
            @if (Request::is('brainster/admin/panel'))
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('main') }}">
                    <img class="logo" src="{{ asset('images/brainster_logo.png') }}" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    @guest
                    <li class="last" data-toggle="modal" data-target="#signUpModal"><a href="#">Sign Up / Sign In</a></li>
                    @else 
                    <li class="nav-item dropdown btnLi">
                        <a id="navbarDropdown" class="btn-custom nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest  
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
            {{-- navbar of the other pages --}}
            @else
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('main') }}">
                    <img class="logo" src="{{ asset('images/brainster_logo.png') }}" alt="">
                </a>
            </div>
        
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav left-nav">
                    <li class="active"><a href="#"><i class="fas fa-code"></i> Programming</a></li>
                    <li><a href="#"><i class="fas fa-database"></i> Data Science</a></li>
                    <li><a href="#"><i class="fas fa-infinity"></i> DevOps</a></li>
                    <li><a href="#"><i class="fas fa-pencil-alt"></i> Design</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @guest
                    <li><a href="#" data-toggle="modal" data-target="#signUpModal"><i style="color: blue;" class="fas fa-plus"></i> Submit a tutorial</a></li>
                    @else
                    <li><a href="#" data-toggle="modal" data-target="#submitModal"><i style="color: blue;" class="fas fa-plus"></i> Submit a tutorial</a></li>
                    @endguest
                    
                    @guest
                    <li class="last" data-toggle="modal" data-target="#signUpModal"><a href="#">Sign Up / Sign In</a></li>
                    @else 
                    <li class="nav-item dropdown btnLi">
                        <a id="navbarDropdown" class="btn-custom nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest  
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
       
            @if (count($errors) > 0)
                <div class="alert alert-danger text-center" id="errorDiv">
                    @foreach ($errors->all() as $error)
                        <p><i class="fas fa-exclamation-circle"></i> {{ $error }}</p>
                    @endforeach
                </div>
            @elseif (session('message'))
                <div class="alert alert-success text-center" id="errorDiv">
                    <p><i class="fas fa-check-circle"></i> {{session("message")}}</p>
                </div>
            @endif

        @include('layouts.modal-submit')
        @include('layouts.modal-signup')
        @include('layouts.modal-signin')
        @endif