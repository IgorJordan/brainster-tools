@extends('layouts.project')

@section('content')
<div class="container course-page">
    <div class="row">
        <div class="col-md-3 side-bar">
            <p class="side-h"><b>Filter Courses</b></p>

            <p><b>Type of course</b></p>
            @foreach ($types as $type)
            <div class="checkbox filterCourse">
                <label for="" id="{{ $type->attribute }}">
                    <input type="checkbox" name="" > {{ $type->name }} 
                    (<span></span>)
                </label>
            </div>
            @endforeach

            <p><b>Medium</b></p>
            @foreach ($media as $medium)
            <div class="checkbox filterCourse">
                <label for="" id="{{ $medium->attribute }}">
                    <input type="checkbox" name=""> {{ $medium->name }} 
                    (<span></span>)
                </label>
            </div>
            @endforeach

            <p><b>Level</b></p>
            @foreach ($levels as $level)
            <div class="checkbox filterCourse">
                <label for="" id="{{ $level->attribute }}">
                    <input type="checkbox" name=""> {{ $level->name }} 
                    (<span></span>)
                </label>
            </div>
            @endforeach
            
            <p><b>Version</b></p>
            @foreach ($versions as $version)
            <div class="checkbox filterCourse" id="versionId">
                <label for="" id="{{ $version->attribute }}">
                    <input type="checkbox" name=""> {{ $version->name }} 
                    (<span></span>)
                </label>
            </div>
            @endforeach    
        </div>

        <div id="courseList" class="col-md-9">
            <div class="title course-side">
                <p><b>Top {{ $tech->name }} tutorials</b></p>
                <div class="filter-butons">
                    <a href="">Upvotes</a>
                    <a class="btn-2" href="">Recent</a>
                </div>
            </div>
            @foreach ($courses as $course)
            <div class="course-card course-side {{ $course->type->attribute }} {{ $course->level->attribute ?? ''}} {{ $course->medium->attribute ?? '' }} {{ $course->version->attribute ?? ''}}">
                <div class="col-md-1">

                    <div class="btn-group square" data-toggle="buttons">
                        <label class="btn btn-primary active">
                            <input type="checkbox" name="" id="" autocomplete="off">
                            <i class="fas fa-caret-up"></i>
                            <h4>{{ $course->likes->count() }}</h4>
                        </label>
                    </div>
                </div>
                <div class="col-md-11 course-pad">
                    <a class="course-title" href="{{ $course->url }}" target="_blank">
                        <h4>{{ $course->title }} 
                            <span class="grey">({{ $course->platform }})</span>
                        </h4>
                    </a>
                    <div class="info-part">
                        <span><a href=""><i class="fas fa-bookmark"></i><b> Bookmark</b></a></span>
                        <span>Submited by <a href=""><b>{{ $course->user->name }}</b></a></span>
                        <span><i class="fas fa-genderless"></i></span>
                        <span>Views</span>
                        <span><i class="fas fa-genderless"></i></span>
                        <span>Discuss</span>
                    </div>
                    <div class="tags">
                        <a href=""><b>{{ $course->type->name }}</b></a>
                        <a href=""><b>{{ $course->level->name ?? ''}}</b></a>
                        <a href=""><b>{{ $course->medium->name ?? '' }}</b></a>
                        <a href=""><b>{{ $course->version->name ?? ''}}</b></a>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="padding text-center" id="padBtn">
                <a href="#" class="prev btn btn-pad">Previous page</a>
                <a href="#" class="next btn btn-pad">Next page</a>
            </div>
        </div> 
    </div>
    <div class="row opt">
        <div class="col-md-9 col-md-offset-3">
            <div class="row">
                <h4 style="margin-bottom: 4vh;">You might also be interested in:</h4>
            </div>
            <div class="row">
                @foreach ($techesLimit as $tech)
                <a href="{{ route('courses', ['id' => $tech->id]) }}">
                <div class="col-md-4">
                    <div class="card">
                        <img class="card-logo" src="{{ asset("images/logos/$tech->logo") }}" alt="program_image">
                        <span> {{ $tech->name }}</span>
                    </div>
                </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection