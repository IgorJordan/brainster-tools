<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tech extends Model
{
    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function versions()
    {
        return $this->hasMany(Version::class);
    }
}
