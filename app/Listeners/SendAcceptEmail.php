<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\AcceptEmail;
use Illuminate\Support\Facades\Mail;

class SendAcceptEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AcceptEmail $event)
    {
        $data['username'] = $event->user->name;
        $data['status'] = 'accepted';
        $data['style'] = 'green';
        $user = $event->user;

        Mail::send('email.confirmation', $data, function ($email) use($user) {
            $email->from('no-reply@brainster.org')->subject('Confirmation E-mail')->to($user->email);
        });
    }
}
