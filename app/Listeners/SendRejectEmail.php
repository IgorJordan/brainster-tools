<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\RejectEmail;
use Illuminate\Support\Facades\Mail;

class SendRejectEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(RejectEmail $event)
    {
        $data['username'] = $event->user->name;
        $data['status'] = 'rejected';
        $data['style'] = 'red';
        $user = $event->user;

        Mail::send('email.confirmation', $data, function ($email) use($user) {
            $email->from('no-reply@brainster.org')->subject('Confirmation E-mail')->to($user->email);
        });

    }
}
