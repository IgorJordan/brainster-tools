<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tech;
use App\Type;
use App\Medium;
use App\Level;
use App\Version;

class TechController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teches = Tech::all();
        $types = Type::all();
        $media = Medium::all();
        $levels = Level::all();
        $versions = Version::all();

        return view('index', compact('types', 'media', 'levels', 'versions', 'teches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function action(Request $request)
    {
        $techs = Tech::where('name', 'like', '%'.$request->get('searchQuery').'%')->get();
        return json_encode($techs);
    }

    public function dynamical(Request $request){
        $data = Version::select('name', 'id')->where('tech_id', $request->id)->get();
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
