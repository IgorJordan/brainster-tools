<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Tech;
use App\Course;
use App\Events\RejectEmail;
use App\Events\AcceptEmail;
use App\Type;
use App\Medium;
use App\Level;
use App\Version;

class AdminController extends Controller
{
    public function show()
    {
        $courses = Course::orderBy('status', 'desc')->paginate(20);

        return view('admin-panel', compact('courses'));
    }

    public function accept($id)
    {
        $course = Course::find($id);
        $course->status = 'accepted';
        $course->save();

        $user = User::where('id', $course->user_id)->first();

        event( new AcceptEmail($user) );

        return redirect()->route('adminPanel');
    }

    public function reject($id)
    {
        $course = Course::find($id);
        $course->status = 'rejected';
        $course->save();

        $user = User::where('id', $course->user_id)->first();

        event( new RejectEmail($user) );

        return redirect()->route('adminPanel');
    }

    public function delete($id)
    {
        $course = Course::find($id);
        
        if(!empty($course))
        {
            $course->delete();
        }
        
        return redirect()->route('adminPanel');

    }
}
