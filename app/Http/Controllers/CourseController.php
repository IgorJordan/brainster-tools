<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\CourseRequest;
use App\Tech;
use App\Course;
use App\Type;
use App\Medium;
use App\Level;
use App\Version;
use App\User;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $courses = Course::where('tech_id', $id)->where('status', 'accepted')->get();
        $types = Type::all();
        $media = Medium::all();
        $levels = Level::all();
        $versions = Version::where('tech_id', $id)->get();
        $teches = Tech::all();
        $techesLimit = Tech::all()->take(6);
        $tech = Tech::where('id', $id)->first();

        return view('courses', compact('courses', 'types', 'media', 'levels', 'versions', 'teches', 'techesLimit', 'tech'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {
        $course = new Course();
        $course->title = $request->title;
        $course->platform = $request->platform;
        $course->url = $request->url;
        $course->tech_id = $request->tech;
        $course->type_id = $request->type;
        $course->medium_id = $request->medium;
        $course->level_id = $request->level;
        $course->version_id = $request->version;
        $course->user_id = auth()->user()->id;
        $course->status = 'unresponded';
        $course->save();

        return redirect()->back()->with('message', 'The course/tutorial is successfully save');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
