<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function tech() 
    {
        return $this->belongsTo(Tech::class);
    }

    public function level() 
    {
        return $this->belongsTo(Level::class);
    }

    public function medium() 
    {
        return $this->belongsTo(Medium::class);
    }

    public function type() 
    {
        return $this->belongsTo(Type::class);
    }

    public function version() 
    {
        return $this->belongsTo(Version::class);
    }

    public function user() 
    {
        return $this->belongsTo('\App\User', 'user_id');
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }
}
