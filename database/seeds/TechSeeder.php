<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TechSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teches')->delete();
        DB::table('teches')->insert([

            [
                'name' => 'Python',
                'logo' => 'python.jpg',
            ],
            [
                'name' => 'JavaScript',
                'logo' => 'js.png',
            ],
            [
                'name' => 'Android Development',
                'logo' => 'android.png',
            ],
            [
                'name' => 'Java',
                'logo' => 'java.png',
            ],
            [
                'name' => 'Data Structures and Algorithms',
                'logo' => 'data-structures.png',
            ],
            [
                'name' => 'C++',
                'logo' => 'c-2plus.png',
            ],
            [
                'name' => 'React',
                'logo' => 'react.png',
            ],
            [
                'name' => 'Angular',
                'logo' => 'angular.jpg',
            ],
            [
                'name' => 'HTML 5',
                'logo' => 'html5.jpg',
            ],
            [
                'name' => 'CSS',
                'logo' => 'css.png',
            ],
            [
                'name' => 'C',
                'logo' => 'c.png',
            ],
            [
                'name' => 'Node.js',
                'logo' => 'node-js.png',
            ],
            [
                'name' => 'PHP',
                'logo' => 'php.png',
            ],
            [
                'name' => 'Git',
                'logo' => 'github.png',
            ],
            [
                'name' => 'Bootstrap',
                'logo' => 'bootstrap.jpg',
            ],
            [
                'name' => 'C#',
                'logo' => 'c-hashtag.png',
            ],
            [
                'name' => 'Information Security & Ethical Hacking',
                'logo' => 'information-security.jpg',
            ],
            [
                'name' => 'Blockchain Programming',
                'logo' => 'blockchain.jpg',
            ],
            [
                'name' => 'MySQL',
                'logo' => 'mysql.jpg',
            ],
            [
                'name' => 'Django',
                'logo' => 'django.png',
            ],
            [
                'name' => 'SQL',
                'logo' => 'sql.png',
            ],
        ]);

    }
}
