<?php

use Illuminate\Database\Seeder;
use App\Like;

class LikeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 150; $i++) { 

            $like = new Like();
            $like->user_id = rand(1, 60);
            $like->course_id = rand(1, 250);
            $like->save();
        }
    }
}
