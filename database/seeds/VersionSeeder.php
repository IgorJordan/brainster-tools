<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VersionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('versions')->delete();
        DB::table('versions')->insert([

            [
                'name' => 'Python 2',
                'attribute' => 'python2',
                'tech_id' => '1',
            ],
            [
                'name' => 'Python 3',
                'attribute' => 'python3',
                'tech_id' => '1',
            ],
            [
                'name' => 'Data Science',
                'attribute' => 'dataScience1',
                'tech_id' => '1',
            ],
            [
                'name' => 'ES6',
                'attribute' => 'es6',
                'tech_id' => '2',
            ],
            [
                'name' => 'Ajax',
                'attribute' => 'ajax',
                'tech_id' => '2',
            ],
            [
                'name' => 'Android 7',
                'attribute' => 'android7',
                'tech_id' => '3',
            ],
            [
                'name' => 'Android 0',
                'attribute' => 'android0',
                'tech_id' => '3',
            ],
            [
                'name' => 'Java 11',
                'attribute' => 'java11',
                'tech_id' => '4',
            ],
            [
                'name' => 'Java Spring Framework',
                'attribute' => 'jsf',
                'tech_id' => '4',
            ],
            [
                'name' => 'JavaEE',
                'attribute' => 'javaEE',
                'tech_id' => '4',
            ],
            [
                'name' => 'Java 9',
                'attribute' => 'java9',
                'tech_id' => '4',
            ],
            [
                'name' => 'Java',
                'attribute' => 'java',
                'tech_id' => '5',
            ],
            [
                'name' => 'Python',
                'attribute' => 'python',
                'tech_id' => '5',
            ],
            [
                'name' => 'JavaScript',
                'attribute' => 'js',
                'tech_id' => '5',
            ],
            [
                'name' => 'C++',
                'attribute' => 'c2Plus',
                'tech_id' => '5',
            ],
            [
                'name' => 'OpenGL',
                'attribute' => 'openGL',
                'tech_id' => '6',
            ],
            [
                'name' => 'C++ Standard Library',
                'attribute' => 'c2PlusSL',
                'tech_id' => '6',
            ],
            [
                'name' => 'Game Development',
                'attribute' => 'gameDevelopment',
                'tech_id' => '6',
            ],
            [
                'name' => 'React 16',
                'attribute' => 'react16',
                'tech_id' => '7',
            ],
            [
                'name' => 'Redux',
                'attribute' => 'redux',
                'tech_id' => '7',
            ],
            [
                'name' => 'GraphQL',
                'attribute' => 'graphQL',
                'tech_id' => '7',
            ],
            [
                'name' => 'Next JS',
                'attribute' => 'nextJS',
                'tech_id' => '7',
            ],
            [
                'name' => 'Node.js',
                'attribute' => 'nodejs',
                'tech_id' => '7',
            ],
            [
                'name' => 'AngularJS',
                'attribute' => 'angularJS',
                'tech_id' => '8',
            ],
            [
                'name' => 'Angular 7',
                'attribute' => 'angular7',
                'tech_id' => '8',
            ],
            [
                'name' => 'Angular 6',
                'attribute' => 'angular6',
                'tech_id' => '8',
            ],
            [
                'name' => 'Angular 5',
                'attribute' => 'angular5',
                'tech_id' => '8',
            ],
            [
                'name' => 'Angular 4',
                'attribute' => 'angular4',
                'tech_id' => '8',
            ],
            [
                'name' => 'CSS',
                'attribute' => 'css',
                'tech_id' => '9',
            ],
            [
                'name' => 'HTML 5',
                'attribute' => 'html5',
                'tech_id' => '9',
            ],
            [
                'name' => 'FLexbox',
                'attribute' => 'flexbox',
                'tech_id' => '10',
            ],
            [
                'name' => 'CSS3',
                'attribute' => 'css3',
                'tech_id' => '10',
            ],
            [
                'name' => 'C',
                'attribute' => 'cID',
                'tech_id' => '11',
            ],
            [
                'name' => 'MERN',
                'attribute' => 'mern',
                'tech_id' => '12',
            ],
            [
                'name' => 'MEAN',
                'attribute' => 'mean',
                'tech_id' => '12',
            ],
            [
                'name' => 'MongoDB',
                'attribute' => 'mongoDB',
                'tech_id' => '12',
            ],
            [
                'name' => 'Node.ja 9',
                'attribute' => 'nodejs9',
                'tech_id' => '12',
            ],
            [
                'name' => 'PHP 7',
                'attribute' => 'php7',
                'tech_id' => '13',
            ],
            [
                'name' => 'MySQL',
                'attribute' => 'mysql',
                'tech_id' => '13',
            ],
            [
                'name' => 'Bootstrap 4',
                'attribute' => 'bootstrap4',
                'tech_id' => '15',
            ],
            [
                'name' => 'Functional Programming',
                'attribute' => 'funcProgramming',
                'tech_id' => '16',
            ],
            [
                'name' => 'C#',
                'attribute' => 'cHushTag',
                'tech_id' => '16',
            ],
            [
                'name' => 'Kali Linux',
                'attribute' => 'kaliLinux',
                'tech_id' => '17',
            ],
            [
                'name' => 'Artificial Intelligence',
                'attribute' => 'artificial',
                'tech_id' => '17',
            ],
            [
                'name' => 'SSCP',
                'attribute' => 'sscp',
                'tech_id' => '17',
            ],
            [
                'name' => 'API',
                'attribute' => 'api',
                'tech_id' => '17',
            ],
            [
                'name' => 'Solidity',
                'attribute' => 'solidity',
                'tech_id' => '18',
            ],
            [
                'name' => 'Bitcoin',
                'attribute' => 'bitcoin',
                'tech_id' => '18',
            ],
            [
                'name' => 'SQL',
                'attribute' => 'sql',
                'tech_id' => '19',
            ],
            [
                'name' => 'Django 1.10',
                'attribute' => 'django110',
                'tech_id' => '20',
            ],
            [
                'name' => 'Django 1.11',
                'attribute' => 'django111',
                'tech_id' => '20',
            ],
            [
                'name' => 'Django 2.0',
                'attribute' => 'django20',
                'tech_id' => '20',
            ],
            [
                'name' => 'Django 3.0',
                'attribute' => 'django30',
                'tech_id' => '20',
            ],
            [
                'name' => 'Vue.js',
                'attribute' => 'vuejs',
                'tech_id' => '20',
            ],
            [
                'name' => 'PL/SQL',
                'attribute' => 'plsql',
                'tech_id' => '21',
            ],
            [
                'name' => 'Data Science',
                'attribute' => 'dataScience2',
                'tech_id' => '21',
            ],
        ]);
    }
}
