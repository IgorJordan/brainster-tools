<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MediumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media')->delete();
        DB::table('media')->insert([

            [
                'name' => 'Book',
                'attribute' => 'book'
            ],
            [
                'name' => 'Video',
                'attribute' => 'video'
            ],
        ]);
    }
}
