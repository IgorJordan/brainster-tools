<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i=0; $i < 60; $i++) { 

            $faker = Faker::create();
            $user = new User();
            $user->name = $faker->userName;
            $user->email = $faker->email;
            $user->password = $faker->password;
            $user->save();
        }


    }
}
